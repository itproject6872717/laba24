﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace game_lab_24
{

    public partial class FallBlock : Form
    {
        int max_points = 0;
        decimal current_score = 0;
        Random rnd = new Random();
        bool game_over = true;
        int line_width = 90;
        int score_multiplier = 1;
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (!game_over)
            {
                if (e.KeyCode == Keys.Left)
                {
                    if (player.Left - line_width >= 0)
                    {
                        player.Left -= line_width;
                    }
                }
                else if (e.KeyCode == Keys.Right)
                {
                    if (player.Left + line_width <= 900)
                    {
                        player.Left += line_width;
                    }
                }
            }
        }
        public FallBlock()
        {   
            InitializeComponent();
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
            timer2.Enabled = true;
            timer2.Interval = 100;
            obstacle_1.Width = 100;
            obstacle_1.Height= 100;
            player.Left = 0;
            obstacle_1.Top = 0;
            obstacle_1.Left = generateObstacle();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public int generateObstacle() //Генерує в рандомній лінії
        {
            int lane = rnd.Next(0, 6);
            return lane * 180;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (game_over)
            {
                timer2.Stop();

            }
            else
            {
                obstacle_1.Top += 20*score_multiplier;
                if (obstacle_1.Top > 435) // Доторкається землі
                {
                    obstacle_1.Top = 0;
                    obstacle_1.Left = generateObstacle();
                    current_score++;
                    score_multiplier = (int)Math.Ceiling(current_score / 5);
                    multi_label.Text = $"Мультиплікатор {score_multiplier}х";
                    score_label.Text = $"Очки: {current_score}";
                }
                if (obstacle_1.Left == player.Left && obstacle_1.Top > 380)//дотикається персонажа
                {
                    game_over = true;
                    panel2.Visible = true;
                    if(Convert.ToInt32(current_score) > max_points)
                    {
                        max_points = Convert.ToInt32(current_score);
                    }
                    max_score.Text = $"Max score = {max_points}";
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            current_score = 0;
            obstacle_1.Top = 0;
            obstacle_1.Left = generateObstacle();
            score_multiplier = 1;
            game_over = false;
            player.Left = 0;
            panel2.Visible= false;
            multi_label.Text = $"Мультиплікатор {score_multiplier}х";
            score_label.Text = $"Очки: {current_score}";
            timer2.Start();
        }
    }
}
